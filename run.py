import os
from aiogram import executor
from handlers import dp
from loguru import logger
from datetime import datetime
from data.config import PROGRAM_PATH


def on_startup():
    logger.debug(f"Bot start at {datetime.now()}")


path_logger = os.path.join(PROGRAM_PATH, "logger.log")
logger.add(path_logger,
           encoding="utf-8",
           format="{time} :: {level} :: {message}",
           rotation="250 KB",
           compression="zip")

executor.start_polling(dp, on_startup=on_startup())
