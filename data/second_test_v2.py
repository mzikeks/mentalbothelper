from data.Question import Question

questions_second_test_v2_text = [
    ('Вы всегда стремитесь выглядеть уверенно и непоколебимо, не “раскисать”, не показывать своих слабостей.\n\n'
     'В стрессовых ситуациях вы стараетесь активно проявлять себя, порой даже резко. Иногда ведете себя слишком'
     ' импульсивно и сожалеете о том, что совершили необдуманный поступок или наговорили лишнего на эмоциях.\n\n'
     'Иногда вы можете создавать дискомфорт для других людей в общении.',
     ['Что с этим делать?']),
    ('Рекомендация: Вам нужно понять логику воздействия вас и окружающих людей друг на друга и изучить'
     ' пространственные правила, чтобы контролировать ход беседы.\n\nВажно не только не поддаваться влиянию,'
     ' но и не давить на других слишком сильно.\n\nПонимаете, о чем я? Давайте разберем наглядно на примере.',
     ['Давай, очень интересно']),
    ('Как-то я три раза встречался с коллегой для обсуждения деловых вопросов в одном и том же помещении. '
     'Две встречи не увенчались успехом, мы так и не смогли договориться.\n\nЯ ощущал с ее стороны какой-то '
     'необъяснимый дискомфорт и стресс. На третьей встрече я задумался - что может быть причиной. '
     'Что мешает ей сконцентрироваться и определиться? И я понял, что нужно было поменять одну деталь при рассадке.'
     '\n\nСработало. Мы наконец приняли решение.\n\nКак думаете, что это была за деталь?',
     ['Открытая дверь за спиной', 'Кондиционер, дующий в лицо',
      'Дым сигарет с соседнего стола', 'Слишком мягкое кресло']),
    ('Когда мы сидели в помещении, моя коллега оба раза сидела спиной к открытой двери, '
     'где постоянно ходили люди, что создавало дискомфорт для нее. Она постоянно отвлекалась и дергалась.\n\n'
     'Я предложил ей пересесть на другое место, где сзади была стена, и разговор наконец пошел продуктивно.\n\n'
     '👆 Иногда куда важнее создать комфортные условия не для себя, а для другого человека,'
     ' который чувствует себя менее уверенно.',
     ['Понятно! Хочу ещё разбор!']),

    (['https://bh-production-file-bucket.storage.yandexcloud.net/mentalmarathon/d4/'
      'd4e2/d4e23b5537a17ec71a809cc0f72632ba/test.jpg',
      'Попробуйте теперь сами.\n\nДопустим, вы начальник и вам предстоит напряженный разговор с подчиненным. '
      'Вы хотите высказать ему критику по некоторым вопросам. Человек на картинке - это вы.'
      '\n\nНа какое место предложите сесть подчиненному?'], ['А', 'Б', 'В', 'Г']),
    ([('👍 Отличный выбор! Это рассадка называется “переговорная”, она хорошо подходит для делового общения и '
       'не настраивает собеседников друг против друга.', ['А что ещё влияет?']),
      ('Не очень удачный выбор. Скорее всего, встреча пройдет напряженно и ваша критика будет восприниматься очень '
       'остро.\n\nЗдесь отлично подойдет вариант "А". Эта рассадка называется "переговорная", она отлично подходит '
       'для делового общения и не настраивает собеседников друг против друга.', ['Хочу знать больше!']),
      ('Не самое удобное положение для продуктивного разговора. Здесь отлично подойдет вариант "А". '
       'Эта рассадка называется "переговорная", она отлично подходит для делового общения и не настраивает '
       'собеседников друг против друга.)', ['Хочу знать больше!']),
      ('Не самое удачное положение для делового общения. Тем более, если хотите, чтобы ваша критика была услышана.'
       '\n\nЗдесь отлично подойдет вариант "А". Эта рассадка называется "переговорная", она отлично подходит для '
       'делового общения и не настраивает собеседников друг против друга.', ['Хочу знать больше!'])]),
    ('То, что мы разобрали - лишь небольшая часть факторов, которые могут влиять на наш комфорт и уверенность и ход '
     'разговора. \n\nВсе эти факторы я подробно разобрал в новом курсе “Стрессовая коммуникация”.\n\nЕсли вы:\n'
     '- Испытываете стресс до и во время важной встречи\n'
     '- Не знаете, как правильно подготовиться к сложному разговору\n'
     '- Не понимаете, как вести себя в сложных переговорах\n'
     '- Теряетесь, когда собеседник давит авторитетом и опытом\n'
     '- В стрессовых ситуациях теряете контроль над эмоциями\n'
     '- Прячете свою естественность за масками, чтобы казаться уверенней\n\n'
     '... то этот мини-курс для ВАС!\n\n'
     'В результате прохождения вы научитесь продуктивно проводить любые напряженные встречи и переговоры и грамотно '
     'использовать окружающее пространство (даже на чужой территории).\n\nВы получите психологическое преимущество, '
     'будете управлять ситуацией и видеть больше, чем другие.\n\n'
     'Помните, я обещал вам бонус после прохождения теста на стрессоустойчивость?\n\n'
     '❗️Именно тем особенным людям, кто готов научиться управлять сложными разговорами, справляться со стрессом и '
     'чувствовать себя уверенно, я дарю скидку 50% на свой авторский курс "Стрессовая коммуникация" — всё это '
     'действует только первые 2 дня ❗️\n\n'
     'P.S. Цена вас приятно удивит 😉\n\n\n👉 Подробнее о курсе - на сайте: '
     'https://clck.ru/U8umJ',
     ['Смотреть программу курса'
      'ref=https://mentalroom.ru/stress-communication?utm_source=telegram&utm_medium=bot&utm_campaign=group_b'])
]

questions_second_test_v2 = []
for i, question_text in enumerate(questions_second_test_v2_text):
    if type(question_text[0]) == tuple and type(question_text[0][1]) == list:
        questions_second_test_v2.append([])
        for j, question_text_true in enumerate(question_text):
            question = Question(question_text_true[0], question_text_true[1],
                                str(i + 1) + '-' + str(j + 1), test_name='second_test_v2')
            questions_second_test_v2[i].append(question)
        continue

    question = Question(question_text[0], question_text[1], i + 1, test_name='second_test_v2')
    questions_second_test_v2.append(question)
