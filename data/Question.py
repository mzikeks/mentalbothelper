from aiogram.types import InlineKeyboardMarkup, InlineKeyboardButton
import random


class Question:
    text = "Question text"
    questions_text = []
    question_number = -1
    test_name = None

    def __init__(self, text, questions_text, question_number, test_name='main_test'):
        self.text = text
        self.questions_text = questions_text
        self.question_number = question_number
        self.test_name = test_name

    def get_message(self):
        markup = InlineKeyboardMarkup(row_width=1)
        current_text = self.text
        if type(self.text) != list:
            current_text += '\n\n'

        if self.test_name != 'main_test':
            shuffled_questions_text = self.questions_text
        else:
            shuffled_questions_text = sorted(self.questions_text, key=lambda k: random.random())

        for answer_n, question_text in enumerate(shuffled_questions_text):
            if self.test_name == 'main_test': # Ответы в тексте вопроса
                markup.add(InlineKeyboardButton(text=str(answer_n+1),
                                                callback_data=f'{self.test_name}_q{self.question_number}_'
                                                              f'a{self.questions_text.index(question_text)}'))
                current_text += f'{answer_n+1}. {question_text}\n'
            else:
                if 'ref=' in question_text:
                    markup.add(InlineKeyboardButton(text=question_text.split('ref=')[0],
                                                    url=question_text.split('ref=')[1]))
                else:
                    markup.add(InlineKeyboardButton(text=question_text,
                                                    callback_data=f'{self.test_name}_q{self.question_number}_'
                                                                  f'a{self.questions_text.index(question_text)}'))
        return current_text, markup
