from data.Question import Question

questions_main_test_text = \
            [('В этом и в следующих вопросах выбирайте из всех вариантов один, который максимально близок для вас.'
              '\n\n1. Что, по-вашему, движет человеком в жизни прежде всего?',
              ['Любопытство',
               'Желания',
               'Необходимость']),
             ('2. Как вы думаете, почему люди переходят с одной работы на другую?',
              ['Они уходят из-за большей зарплаты',
               'Другая работа им больше по душе',
               'Их увольняют']),
             ('3. Когда у вас происходят неприятности:',
              ['Вы откладываете их решение до последнего',
               'Вы хотите сразу же разобраться и отстоять свои интересы',
               'Вы не хотите даже и думать о том, что случилось']),
             ('4. Вы не успели вовремя сделать какую-то работу и:',
              ['Вы основательно подготавливаетесь к объяснению',
               'Заявляете сами о своей неудаче еще до того, как это откроется',
               'С боязнью ждете, когда вас спросят о результатах']),
             ('5. Когда вы достигаете какой-то поставленной цели, то встречаете известие об этом:',
              ['По-разному, в зависимости от цели, но не так бурно',
               'С бурными положительными эмоциями',
               'С чувством облегчения']),
             ('6. Что бы вы рекомендовали очень стеснительному человеку?',
              ['Познакомиться с людьми другого склада, не страдающими застенчивостью',
               'Избавиться от этого, обратившись к помощи психолога',
               'Избегать ситуаций, требующих риска']),
             ('7. Как вы поступите в конфликтной ситуации?',
              ['Напишу письмо',
               'Поговорю с тем, с кем вступил в конфликт',
               'Попробую разрешить конфликт через посредника']),
             ('8. Какого рода страх возникает у вас, когда вы ошибаетесь?',
              ['Страх того, что ошибка может изменить тот порядок, к которому вы привыкли',
               'Боязнь потерять престиж',
               'Боязнь наказания']),
             ('9. Когда вы с кем-то разговариваете, то:',
              ['Время от времени отводите взгляд',
               'Смотрите прямо в глаза собеседнику',
               'Отводите взгляд, даже когда к вам обращаются']),
             ('10. Когда вы ведете важный разговор, то:',
              ['Вы то и дело вставляете ничего не значащие слова',
               'Тон разговора обычно остается спокойным',
               'Вы волнуетесь, заикаетесь, голос начинает вас подводить'])]

questions_main_test = []
for i, question_text in enumerate(questions_main_test_text):
    question = Question(question_text[0], question_text[1], i+1, test_name='main_test')
    questions_main_test.append(question)
