from data.Question import Question

questions_finish_test_text = [
    ('Привет! Вчера мы с вами обсуждали, как повысить уверенность и комфорт в ситуациях стрессовой коммуникации.\n\n'
     'А как вы думаете - почему вообще люди ведут себя доминантно или неуважительно по отношению к нам?',
     ['Они плохие', 'Мы позволяем им себя так вести', 'Сегодня неудачный день', 'Такова суровая жизнь']),
    ('На самом деле, это всё потому, что мы позволяем такое поведение по отношению к нам. И позволяем ставить '
     'себя в условия, неудобные для нас.\n\n🤔 Как обычно выглядит ваш процесс коммуникации?\n\n'
     'Заходите в помещение, садитесь на место, куда падает взгляд, общаетесь словно "по течению". Когда что-то '
     'беспокоит - концентрируетесь на этом чувстве и не можете от него избавиться? А если чувствуете дискомфорт или '
     'давление на себя, то не знаете, что с этим делать?\n\nВ общем, скорее всего, вы ведете себя бессистемно и '
     'автоматически, как подсказывают чувства и тело.\n\nНе удивительно, что у вас постоянно возникает стресс при '
     'общении с новыми людьми, в незнакомых местах и ситуациях. Особенно рядом с людьми выше по статуcу...\n\n'
     '❓ Бывает такое?', ['Да, знакомое чувство']),
    ('Я глубоко изучил этот вопрос и разобрался, как поведение “уверенных” людей может влиять на нас,'
     ' и что делать, чтобы держаться на уровне, не поддаваться их доминированию и отстаивать свои интересы.\n\nОб'
     ' этих техниках я подробно рассказываю на курсе “Стрессовая коммуникация”.\n\n👉 Подробнее: https://clck.ru/U8uoG',
     ['Узнать подробнее о курсе'
      'ref=https://mentalroom.ru/stress-communication?utm_source=telegram&utm_medium=bot&utm_campaign=day_2',
      'Уже купил(а)!']),
    (['📣 В течение двух суток можно купить курс по специальной цене для самых решительных и получить БОНУС - мое '
      'интервью с профессиональным игроком в покер на тему “Невербальная коммуникация в покере и в реальной жизни. '
      'Мифы и рабочие техники”.', 'BAACAgIAAxkDAAEKIEtgnRmT7O7UtvaXK7kXesQ8sF5VTwACyg0AAvMI6Uhz1jO64QEYrR8E'], []),
    ('Привет👋 Если вы задумываетесь о приобретении курса “Стрессовая коммуникация”, но что-то до сих пор '
     'останавливает, пишите свои вопросы и сомнения, я на связи и готов ответить!\n\n'
     '🎁 Напоминаю, что только за покупку в течение двух суток мы даем скидку + бонус.',
     ['Перейти на сайт'
      'ref=https://mentalroom.ru/stress-communication?utm_source=telegram&utm_medium=bot&utm_campaign=day_2a',
      'Уже купил(а)!', 'Мне не интересно']),
    ('Скидка и бонус скоро сгорают! Потом не жалуйтесь, что опоздали всего на 5 минут 😃\n\n👉 https://clck.ru/U8upF\n\n'
     'Это было последнее напоминание про курс "Стрессовая коммуникация" 😉\n\nХотите ли получать от нас еще '
     'полезные материалы?', ['Да, конечно!', 'Нет, спасибо!'])]

questions_finish_test = []
for i, question_text in enumerate(questions_finish_test_text):
    question = Question(question_text[0], question_text[1], i+1, test_name='finish_test')
    questions_finish_test.append(question)

purchase = '🎉 Поздравляю!\n\nТогда не теряйте времени - переходите и начинайте учиться! Обязательно выполняйте все ' \
          'задания, для достижения результата от курса очень важно не только изучить теорию, но и внедрить ' \
          'навыки в жизнь.\n\nДо встречи на курсе!'

materials = 'Отлично, тогда оставайтесь с нами, впереди много интересного! 👋'

not_interested = 'Ну что ж, выбор за вами 😊 Всего доброго и спасибо, что провели с нами время! До встречи 👋'

