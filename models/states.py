from aiogram.dispatcher.filters.state import State, StatesGroup


class ChangeEmailState(StatesGroup):
    waiting_for_input_email = State()