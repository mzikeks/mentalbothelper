from peewee import *
import os
from data.config import PROGRAM_PATH


path_db = os.path.join(PROGRAM_PATH, "models", 'data_base.db')
print(os.path.join(PROGRAM_PATH, "models", 'data_base.db'))

db = SqliteDatabase(path_db)


class BaseModel(Model):
    class Meta:
        database = db


class User(BaseModel):
    email = TextField(null=True)
    telegram_id = TextField()
    main_test_answers = TextField(default='')
    current_state = TextField(default='start')
    group = TextField(null=True)
    status = TextField(default='reg')
    first_day_site = BooleanField(default=False)
    purchase = BooleanField(default=False)
    materials = BooleanField(default=False)
    not_interested = BooleanField(default=False)


if __name__ == '__main__':
    db.connect()
    db.create_tables([User])
