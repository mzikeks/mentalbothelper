from loader import dp
from aiogram import types
from models.states import ChangeEmailState
from aiogram.dispatcher.filters import Text
from models.model import User


@dp.message_handler(Text(equals="stop", ignore_case=True), state="*")
@dp.message_handler(Text(equals="стоп", ignore_case=True), state="*")
@dp.message_handler(Text(equals="отписка", ignore_case=True), state="*")
@dp.message_handler(commands=['stop'], state='*')
async def process_start_command(message: types.Message):
    user = User.get_or_none(User.telegram_id == message.from_user.id)
    if user is not None:
        user.status = 'block'
        user.save()
        #User.delete().where(User.telegram_id == message.from_user.id).execute()

    await message.answer(text='Вы отписались от бота. Для повторной подписки нажмите /start')
