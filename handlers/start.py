from loader import dp
from aiogram import types
from aiogram.types import InlineKeyboardMarkup, InlineKeyboardButton, CallbackQuery
from models.model import User
from models.states import ChangeEmailState
from handlers.main_test import start_main_test_with_time
import re
from aiogram.dispatcher.filters import Text
import handlers
from loguru import logger
import asyncio
import os


@dp.message_handler(commands=['debug22891'], state='*')
async def process_start_command(message: types.Message):
    if True:
        return
    with open('C:/Users/Misha/Downloads/Telegram Desktop/Приветствие бот (2).mp4', 'rb') as file:
        msg = await message.answer_video(file, disable_notification=True)
        print(getattr(msg, 'video').file_id)


@dp.message_handler(commands=['start'], state='*')
@dp.message_handler(Text(equals="старт", ignore_case=True), state="*")
@dp.message_handler(Text(equals="начать", ignore_case=True), state="*")
@dp.message_handler(Text(equals="тест", ignore_case=True), state="*")
@dp.message_handler(Text(equals="стресс", ignore_case=True), state="*")
@dp.message_handler(Text(equals="start", ignore_case=True), state="*")
async def process_start_command(message: types.Message):
    state = dp.current_state(user=message.from_user.id)
    await state.set_state(ChangeEmailState.waiting_for_input_email)

    user = User.get_or_none(User.telegram_id == message.from_user.id)
    logger.debug(f'start bot from user with status {"new" if user is None else user.status}')

    if user is None:
        User.create(telegram_id=message.from_user.id, current_state="reg")
    elif user.status == 'block':
        user.main_test_answers = ''
        user.purchase = False
        user.not_interested = False
        user.materials = False
        user.current_state = "email_confirmed"
        user.status = 'reg'
        user.save()
    else:
        await message.answer("Вы уже зарегестрированы в боте")
        return

    await message.answer(text='👋  Привет!\n' 
                         'Оставьте свой email, чтобы в случае каких-то сложностей у нас с вами был способ '
                         'связаться или выслать дополнительные материалы, которые могли бы вам быть интересны')


@dp.message_handler(state=ChangeEmailState.waiting_for_input_email)
async def process_start_command(message: types.Message):
    state = dp.current_state(user=message.from_user.id)

    user = User.get_or_none(User.telegram_id == message.from_user.id)

    if user.status == 'block':
        await message.answer(text='Вы отписаны от бота. Для повторного запуска нажмите /start')
        return

    if not re.match(r"[^@]+@[^@]+\.[^@]+", message.text):
        await message.answer(text='Для продолжения работы с ботом, введите, пожалуйста, корректный email')
        return

    user.email = message.text
    user.status = "email-confirmed"
    user.save()

    await state.finish()

    await message.answer_video('BAACAgIAAxkDAAEKma9gwHtO4os6N2wFWborVPC97rj4YAACQAwAAlJTAAFKmR0vgGiDMlAfBA')
    await asyncio.sleep(5)

    reply_markup = InlineKeyboardMarkup().add(InlineKeyboardButton(text='Получить материалы!',
                                                                   callback_data='start_main_test_materials',
                                                                   url='https://instateleport.ru/page/inst-sk'))
    reply_markup.add(InlineKeyboardButton(text='Перейти к тесту',
                                          callback_data='start_main_test'))

    new_message = await message.answer(
                        text='Спасибо!\n'
                             'Меня зовут Сергей Бубович и я специалист в области межличностной коммуникации и '
                             'социальной инженерии.\n\nЯ более 10 лет изучаю психологию поведения и невербальную '
                             'коммуникацию, а также обучаю управлять общением, добиваться своих целей, защищаться от '
                             'манипуляций и видеть искренние мотивы и намерения людей.\n\nВполне возможно, что вы '
                             'видите меня не впервые, ведь я создатель YouTube-канала MENTAL, — крупнейшего '
                             'русскоязычного канала о невербальной коммуникации.\n\nСейчас я предлагаю вам пройти '
                             'небольшое тестирование, после которого вы узнаете свой уровень уверенности и '
                             'стрессоустойчивости, а также получите рекомендации по поведению в напряжённых ситуациях.'
                             '\n\nКстати, после его прохождения вы получите приятный подарок!\n\nТакже при подписке на '
                             'меня в Инстаграм, я дарю всем чек-лист "Влияние цвета на результат коммуникации" — '
                             'Забирайте, пока он находится в свободном доступе!', reply_markup=reply_markup)
    await start_main_test_with_time(sent_message=new_message, user=user)


@dp.message_handler(state="*")
async def process_start_command(message: types.Message):
    await message.answer(text='🤖 К сожалению, я еще не обучен распознавать письменную речь. '
                              'Чтобы общаться со мной, нажимайте на предложенные кнопки 🙂\n\n'
                              'Если у вас возник вопрос по программе курса «Стрессовая коммуникация», пожалуйста,'
                              ' обратитесь в службу поддержки @Sergey1203')