import asyncio
import aioschedule
from loader import dp
from data.second_test_v1 import questions_second_test_v1
from data.second_test_v2 import questions_second_test_v2
from data.second_test_v3 import questions_second_test_v3
import handlers
from models.model import User
from aiogram.types import InlineKeyboardMarkup, InlineKeyboardButton, CallbackQuery
import datetime
from data.config import k_time
from loguru import logger

questions = [questions_second_test_v1, questions_second_test_v2, questions_second_test_v3]


async def start_second_test(callback_query, test_number):
    await callback_query.message.edit_reply_markup(reply_markup=None)
    if test_number == 1:
        text, markup = questions_second_test_v1[0].get_message()
        variation = 1
    elif test_number == 2:
        text, markup = questions_second_test_v2[0].get_message()
        variation = 2
    else:
        text, markup = questions_second_test_v3[0].get_message()
        variation = 3

    new_message = await callback_query.message.answer(text=text, reply_markup=markup)

    await asyncio.sleep(70)

    user = User.get_or_none(User.telegram_id == callback_query.from_user.id)

    if user.current_state == callback_query.data and user.status != 'block':
        callback_query.data = f'second_test_v{variation}_q1_a0'
        callback_query.message = new_message
        await question_second_test(callback_query)


@dp.callback_query_handler(lambda command: 'second_test_v' in command.data)
async def question_second_test(callback_query: CallbackQuery):
    logger.debug(callback_query.data)
    await callback_query.message.edit_reply_markup(reply_markup=None)

    test_variation = int(callback_query.data.split('_')[2][1:])
    question_number = int(callback_query.data.split('_')[3][1:].split('-')[0])
    answer = int(callback_query.data.split('_')[4][1:])

    user = User.get_or_none(User.telegram_id == callback_query.from_user.id)
    user.current_state = callback_query.data
    user.save()

    if test_variation == 2 and question_number == 5:  # second_test_v2_q5_a1..3
        text, markup = questions[test_variation - 1][question_number][answer].get_message()
    else:
        text, markup = questions[test_variation - 1][question_number].get_message()

    if type(text) == list:
        for i in range(len(text) - 1):
            if 'BAACA' in text[i]:
                await callback_query.message.answer_video(text[i])
            elif '.jpg' in text[i]:
                await callback_query.message.answer_photo(text[i])
            else:
                await callback_query.message.answer(text=text[i], reply_markup=None)
            await asyncio.sleep(5)
        text = text[-1]

    new_message = await callback_query.message.answer(text=text, reply_markup=markup)

    if question_number + 1 == len(questions[test_variation - 1]):
        await finish_second_test(user, callback_query)

    elif len(markup.inline_keyboard) == 1:
        await asyncio.sleep(70)
        user = User.get_or_none(User.telegram_id == callback_query.from_user.id)
        if user.current_state == callback_query.data and user.status != 'block':
            callback_query.data = callback_query.data.replace(f'q{question_number}_a{answer}',
                                                              f'q{question_number+1}_a0')
            callback_query.message = new_message
            await question_second_test(callback_query)


async def finish_second_test(user, callback_query):
    logger.debug("start final test timer")
    await asyncio.sleep(24*60*60 * k_time)
    if datetime.time(10, 00) <= datetime.datetime.now().time() <= datetime.time(22, 00):
        await handlers.finish_test.start_finish_test(user, callback_query)
    else:
        now = datetime.datetime.now()
        # Если сейчас после полуночи, срабатывает второе - сообщение в 10:00
        # Если 22:00 - 24:00, то сообщение будет отправлено в 10 - 12 дня.
        time_to_sleep = max(now - now.replace(hour=10, minute=0), now.replace(hour=10, minute=0)-now) * k_time
        await asyncio.sleep(time_to_sleep.total_seconds())
        await handlers.finish_test.start_finish_test(user, callback_query)
