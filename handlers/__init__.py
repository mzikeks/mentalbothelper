from .stop import dp
from .main_test import dp
from .start import dp
from .second_test import dp
from .finish_test import dp

__all__ = ["dp"]