from loader import dp
import asyncio
from aiogram.dispatcher.filters import Text
from data.finish_test import questions_finish_test, purchase, materials, not_interested
from models.model import User
from aiogram.types import InlineKeyboardMarkup, InlineKeyboardButton, CallbackQuery
from data.config import k_time
from loguru import logger


async def start_finish_test(user, callback_query: CallbackQuery):
    logger.debug('start finish test')
    text, markup = questions_finish_test[0].get_message()

    user = User.get_or_none(User.telegram_id == callback_query.from_user.id)
    if user.status == 'block':
        return

    await callback_query.message.answer(text=text, reply_markup=markup)


@dp.callback_query_handler(lambda command: 'finish_test_q3_a1' in command.data)
@dp.callback_query_handler(lambda command: 'finish_test_q5_a1' in command.data)
async def purchase_final_test(callback_query: CallbackQuery):
    logger.debug(callback_query.data)
    await callback_query.message.edit_reply_markup(reply_markup=None)
    logger.debug(callback_query.data)
    user = User.get_or_none(User.telegram_id == callback_query.from_user.id)
    user.purchase = True
    user.save()

    await callback_query.message.answer(text=purchase)


@dp.callback_query_handler(lambda command: 'finish_test_q6_a0' in command.data)
async def additional_material_final_test(callback_query: CallbackQuery):
    logger.debug(callback_query.data)
    await callback_query.message.edit_reply_markup(reply_markup=None)
    user = User.get_or_none(User.telegram_id == callback_query.from_user.id)
    user.materials = True
    user.save()
    await callback_query.message.answer(text=materials)


@dp.callback_query_handler(lambda command: 'finish_test_q5_a2' in command.data)
@dp.callback_query_handler(lambda command: 'finish_test_q6_a1' in command.data)
async def not_interested_final_test(callback_query: CallbackQuery):
    logger.debug(callback_query.data)
    await callback_query.message.edit_reply_markup(reply_markup=None)
    user = User.get_or_none(User.telegram_id == callback_query.from_user.id)
    user.not_interested = True
    user.save()
    await callback_query.message.answer(text=not_interested)


@dp.callback_query_handler(lambda command: 'finish_test_q1' in command.data)
@dp.callback_query_handler(lambda command: 'finish_test_q2' in command.data)
async def question_final_test(callback_query: CallbackQuery):
    logger.debug(callback_query.data)
    await callback_query.message.edit_reply_markup(reply_markup=None)

    question_number = int(callback_query.data.split('_')[2][1:].split('-')[0])

    user = User.get_or_none(User.telegram_id == callback_query.from_user.id)

    user.current_state = callback_query.data
    user.save()

    text, markup = questions_finish_test[question_number].get_message()
    new_message = await callback_query.message.answer(text=text, reply_markup=markup)

    if question_number == 2:
        await asyncio.sleep(60)

        user = User.get_or_none(User.telegram_id == callback_query.from_user.id)
        if user.purchase or user.status == 'block':
            return
        text, markup = questions_finish_test[question_number+1].get_message()
        await callback_query.message.answer(text=text[0], reply_markup=None)
        await callback_query.message.answer_video(text[1])

        if not user.purchase and not user.not_interested:
            await asyncio.sleep(19*60*60 * k_time)
            text, markup = questions_finish_test[question_number + 2].get_message()
            await callback_query.message.answer(text=text, reply_markup=markup)

            if not user.purchase and not user.not_interested:
                await asyncio.sleep(2*60*60 * k_time)

                user = User.get_or_none(User.telegram_id == callback_query.from_user.id)
                if user.status == 'block':
                    return
                text, markup = questions_finish_test[question_number + 3].get_message()
                await callback_query.message.answer(text=text, reply_markup=markup)

        return

    elif question_number == 1:
        await asyncio.sleep(120)

        user = User.get_or_none(User.telegram_id == callback_query.from_user.id)
        if user.current_state == callback_query.data and user.status != 'block':
            callback_query.data = callback_query.data.replace(f'q{question_number}',
                                                              f'q{question_number + 1}')
            callback_query.message = new_message
            await question_final_test(callback_query)
