import asyncio
from loader import dp
from aiogram.dispatcher.filters import Text
from data.main_test import questions_main_test
from handlers.second_test import start_second_test
from models.model import User
from aiogram import types
from aiogram.types import InlineKeyboardMarkup, InlineKeyboardButton, CallbackQuery


@dp.callback_query_handler(lambda command: 'start_main_test' in command.data)
async def start_main_test(callback_query: CallbackQuery):
    user = User.get_or_none(User.telegram_id == callback_query.from_user.id)

    if 'materials' in callback_query.data:
        user.materials = 'teleport'

    await callback_query.message.edit_reply_markup(reply_markup=None)

    user.current_state = 'start_main_test'
    user.save()

    text, markup = questions_main_test[0].get_message()
    await callback_query.message.answer(text=text, reply_markup=markup)


async def start_main_test_with_time(sent_message: types.Message, user):
    await asyncio.sleep(70)

    user = User.get(User.telegram_id == user.telegram_id)
    if user.current_state != 'email_confirmed' or user.status == 'block':
        return

    await sent_message.edit_reply_markup(reply_markup=None)

    text, markup = questions_main_test[0].get_message()
    await sent_message.answer(text=text, reply_markup=markup)


@dp.callback_query_handler(lambda command: 'main_test_q' in command.data)
async def question_main_test(callback_query: CallbackQuery):
    question_number = int(callback_query.data.split('_')[2][1:])
    answer = int(callback_query.data.split('_')[3][1:])

    user = User.get_or_none(User.telegram_id == callback_query.from_user.id)
    user.main_test_answers += str(answer)
    user.current_state = callback_query.data
    user.save()

    if question_number == 10:
        await main_test_results(list(user.main_test_answers), callback_query, user)
        return

    text, markup = questions_main_test[question_number].get_message()

    await callback_query.message.edit_text(text=text, reply_markup=markup)


async def main_test_results(answers, callback_query, user):
    a_count = answers.count('0')
    b_count = answers.count('1')
    c_count = answers.count('2')

    if c_count >= 5:
        user.group = 'c'
        user.save()
        await start_second_test(callback_query, 3)

    elif b_count >= 7:
        user.group = 'b'
        user.save()
        await start_second_test(callback_query, 2)

    else:
        user.group = 'a'
        user.save()
        await start_second_test(callback_query, 1)
